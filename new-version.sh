#!/bin/bash
if [ -z $1 ]; then
	echo 'Input error.';
	echo 'Usage:';
	echo './new-version.sh VERSIONNAME';
	echo 'VERSIONNAME will be DB user and database name';
	exit -1;
fi
username=$1
sudo -u postgres createuser -U postgres $username
sudo -u postgres createdb -U postgres --owner=$username --encoding=UTF8 $username
echo 'in psql, do this:'
echo "ALTER USER $username WITH PASSWORD 'password';"
read
sudo -u postgres psql
