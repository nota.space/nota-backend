#!/bin/bash
if [ $# -eq 0 ]
then
    echo "Provide filename parameter"
else
    echo "Backing up to '$1'"
    pg_dump -U postgres nota > $1
    #python manage.py dumpdata \
    #    --all --natural-foreign \
    #    --traceback \
    #    --output $1
fi

