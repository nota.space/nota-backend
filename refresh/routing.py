from django.urls import re_path

from . import consumers

refresh_urlpatterns = [
    re_path(r'ws/refresh/(?P<space_name>\w+)/$', consumers.RefreshConsumer.as_asgi()),
]
