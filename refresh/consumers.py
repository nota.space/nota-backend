import sys
import json
import asyncio
import logging
import traceback
from datetime import datetime
from asgiref.sync import sync_to_async
from .exceptions import ClientError
from channels.generic.websocket import AsyncJsonWebsocketConsumer
from channels.generic.websocket import JsonWebsocketConsumer

from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.authtoken.models import Token

@sync_to_async
def get_token(user_id):
    return Token.objects.get(user=user_id)

class RefreshConsumer(AsyncJsonWebsocketConsumer):
    # keep track of the of active users (by random string)
    active_clients = {}
    tab_id = None
    user_id = None
    token = None

    async def connect(self):
        self.space_name = self.scope['url_route']['kwargs']['space_name']
        self.space_group_name = 'space_%s' % self.space_name
        print("WS    connect: " + self.space_group_name + " by " + self.channel_name)
        sys.stdout.flush()
        await self.channel_layer.group_add(
             self.space_group_name,
             self.channel_name
        )
        await self.channel_layer.group_add(
                "all",
                self.channel_name
        )
        await self.accept()
        self.connected = True
        # start interval task to send connections count
        self.send_connections_task = asyncio.ensure_future(self.send_connections())

    async def disconnect(self, code):
        """
        Called when the WebSocket closes for any reason.
        """
        self.connected = False

        print("WS disconnect: " + self.space_group_name + " by " + self.channel_name)
        sys.stdout.flush()
        try:
            await self.channel_layer.group_discard(
                self.space_group_name,
                self.channel_name
            )
        except Exception as ex:
            logging.error(traceback.format_exc())

        try:
            await self.channel_layer.group_discard(
                    "all",
                    self.channel_name
            )
        except Exception as ex:
            logging.error(traceback.format_exc())

        # Leave all the spaces we are still in
        if self.tab_id is not None:
            try:
                await self.channel_layer.group_send(
                    self.space_group_name,
                    {
                        'type': 'remove.client',
                        'content': {
                            'tab_id': self.tab_id,
                        }
                    }
                )
            except Exception as ex:
                logging.error(traceback.format_exc())

    async def check_auth(self, user_id, token):
        return (
                token is not None and user_id is not None and
                self.token == token and self.user_id == user_id
        )

    async def receive_json(self, content):
        """
        Called when we get a text frame. Channels will JSON-decode the payload
        for us and pass it as the first argument.
        """
        # Messages will have a "command" key we can switch on
        cmdtype = content.get("type", None)
        content_type = content.get("content_type", None)
        content['group_name'] = self.space_group_name
        content['source'] = self.channel_name
        user_id = content.get('user_id')
        token = content.get('token')
        tab_id = content.get('tab_id')
        is_authenticated = False

        if self.check_auth(user_id, token):
            is_authenticated = True
        else:
            # this can also happen if auth message has not arrived
            is_authenticated = False

        # only allow connecting and sharing user view for anonymous users
        if (not is_authenticated and
                cmdtype not in ['c', ] and
                content_type not in ['v']):
            return

        if tab_id is not None:
            await self.channel_layer.group_send(
                'all',
                {
                    'type': 'tab.active',
                    'tab_id': tab_id,
                }
            )

        try:
            if cmdtype == "c":
                # save the clients' tab_id
                self.tab_id = tab_id
            elif cmdtype == "b":
                # just broadcast the message
                await self.channel_layer.group_send(
                    self.space_group_name,
                    {
                        'type': 'broadcast',
                        'content': content
                    }
                )
            elif cmdtype == "auth":
                if (user_id is not None and
                        user_id != 1 and
                        token is not None and
                        token == str(await get_token(user_id))):
                    self.user_id = user_id
                    self.token = token
                    logging.info('websocket authorized for user_id ' + str(user_id))
                else:
                    if self.user_id is not None:
                        logging.info('un-authorize websocket of user_id ' + str(user_id))
                    self.user_id = None
                    self.token = None

            elif type == "u":
                # fragments updated
                pass

        except ClientError as e:
            # Catch any errors and send it back
            await self.send_json({"error": e.code})

    # Receive broadcast from space group
    async def broadcast(self, event):
        content = event['content']
        if self.channel_name != content['source']:

            # Send message to WebSocket
            try:
                await self.send(text_data=json.dumps(
                    content
                ))
            except Exception as ex:
                logging.error(traceback.format_exc())

    # Receive disconnect info from space group
    async def remove_client(self, event):
        content = event['content']
        # type is disconnect
        content['content_type'] = 'd'

        # Send message to WebSocket
        await self.send(text_data=json.dumps(
            content
        ))

    # Receive active tab.active info on 'all' group
    async def tab_active(self, event):
        # record last active time for tab_id
        tab_id = event['tab_id']
        self.active_clients[tab_id] = datetime.now()

    # periodically inform client about active user count
    async def send_connections(self):
        while self.connected:
            try:
                # remove old clients
                now = datetime.now()
                to_delete = []
                for tab_id in self.active_clients:
                    then = self.active_clients[tab_id]
                    seconds_since_active = (now - then).total_seconds()
                    if seconds_since_active > 15:
                        to_delete.append(tab_id)

                for tab_id in to_delete:
                    del self.active_clients[tab_id]

                await self.send_json(
                        {
                            "connections_count": len(self.active_clients),
                        }
                )
                await asyncio.sleep(5.0)
            except Exception as ex:
                logging.error(traceback.format_exc())
                print("WS break loop: " + self.space_group_name + " by " + self.channel_name)
                sys.stdout.flush()
                break
