# Installation

## Mac only

Install General Requirements (on Mac):
```
brew install postgresql
brew install zlib
```

## Install Docker

```
# you could use snap, f.i.
sudo snap install docker
```

## Install PostgreSQL:

Install PostgreSQL, create the user and database needed

commands that might be useful here:
```
sudo apt install postgresql
sudo apt install libpq-dev
```

## Install Python

It is recommended to use pip + env
Python version as of this time: 3.8 (best to check with other developers if this is still true)

commands that might be useful here:
```
# install python system packages
sudo apt install python3
sudo apt install python3-pip
sudo apt install python3-venv
# Create the viratual environment called virtualenv
# within the repository, the virtualenv/ directory is in .gitignore
python3 -m venv virtualenv
# Activate the the virtual evironment:
source virtualenv/bin/activate
# Install our project's requirements
pip install -r requirements.txt
```


# Configuration

## Docker
```
sudo groupadd docker
sudo usermod -aG docker $USER
```

And then *reboot*

## PostgreSQL

Create Postgres User:
```
createuser -s postgres # on Mac
sudo -u postgres createuser -U postgres nota
```

Create Postgres DB:
```
sudo -u postgres createdb -U postgres --owner=nota --encoding=UTF8 nota
```

```
sudo -u postgres psql
ALTER USER nota WITH PASSWORD 'password';
```

## Django

Create the static folder:

```
mkdir static
```
Create a local .env file:
```
cp .env-template .env
```

Then edit the .env file, to match your database setup

Make sure your python virtual environment is active.
Running database setup might be required before running the server.
Migrations are currently weird. One solution is to run them in this
order:
```
python manage.py migrate
python manage.py migrate guardian
python manage.py migrate
python manage.py createsuperuser
```

Then log into django admin, and create a user 'nota' and a space 'default' of that user, with anonymous access

Running for development:
```
# redis in a docker container
./docker.sh
# running the backend server
python manage.py runserver
```
