from accounts.models import User
from .models import Room
from django.db.models.signals import post_save
from django.dispatch import receiver

from guardian.shortcuts import assign_perm

@receiver(post_save, sender=User)
def user_post_save(sender, **kwargs):
    """
    Create empty default space for all new users
    """

    user, created = kwargs["instance"], kwargs["created"]
    if created:
        space_create_data = {
            'name': 'default',
            'created_by': user,
            'anonymous_access': False,
            'admin': user,
        }
        space = Room.objects.create(**space_create_data)
        assign_perm('room_admin', user, space)
