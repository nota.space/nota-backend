# Generated by Django 3.0.8 on 2022-08-23 18:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rooms', '0039_auto_20220731_1811'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='room',
            options={'default_permissions': (), 'permissions': [("room_admin", "Can manage the space"), ("modify_room_fragments", "Can modify fragments of the space"), ("view_room_fragments", "Can view fragments of the space")], 'verbose_name': 'Room', 'verbose_name_plural': 'Rooms'},

        ),
    ]
