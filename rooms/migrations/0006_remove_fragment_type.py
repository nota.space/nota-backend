# Generated by Django 2.1.1 on 2018-11-24 18:34

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rooms', '0005_auto_20181124_1118'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='fragment',
            name='type',
        ),
    ]
