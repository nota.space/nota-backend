from django.db import migrations
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from guardian.shortcuts import assign_perm


permissions = [
  ("room_admin", "Can manage the space"),
  ("modify_room_fragments", "Can modify fragments of the space"),
  ("view_room_fragments", "Can view fragments of the space"),
]


# This migration is for moving room permissions (room admin and room user table) into django object permission model.
# We will delete the table and column after we confirm migration is done.
def forwards(apps, schema_editor):
    # use the historical model
    Room = apps.get_model('rooms', 'Room')
    RoomUser = apps.get_model('rooms', 'RoomUser')

    all_rooms_users = RoomUser.objects.all()
    for ru in all_rooms_users.iterator():
        if ru.can_delete or ru.can_write:
            assign_perm("modify_room_fragments", ru.user, ru.room)
        elif ru.can_read:
            assign_perm("view_room_fragments", ru.user, ru.room)
    all_rooms = Room.objects.all()
    for room in all_rooms.iterator():
        assign_perm("room_admin", room.created_by, room)
        if room.admin and room.created_by.id != room.admin.id:
            assign_perm("room_admin", room.admin, room)


class Migration(migrations.Migration):
    dependencies = [
        ('rooms', '0040_auto_20220823_2005'),
    ]

    operations = [
        migrations.RunPython(forwards),
    ]
