# Generated by Django 2.1.1 on 2019-02-02 15:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rooms', '0020_fragment_censored'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='roomownergroup',
            name='users',
        ),
        migrations.RemoveField(
            model_name='room',
            name='owner_group',
        ),
        migrations.DeleteModel(
            name='RoomOwnerGroup',
        ),
    ]
