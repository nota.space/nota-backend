# Generated by Django 2.1.1 on 2018-12-20 14:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rooms', '0019_auto_20181205_0459'),
    ]

    operations = [
        migrations.AddField(
            model_name='fragment',
            name='censored',
            field=models.BooleanField(default=False),
        ),
    ]
