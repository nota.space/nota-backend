# Generated by Django 2.1.1 on 2018-11-26 13:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rooms', '0010_auto_20181126_1444'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fragment',
            name='url',
            field=models.TextField(blank=True),
        ),
    ]
