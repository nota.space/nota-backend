# Generated by Django 2.1.1 on 2018-11-26 12:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rooms', '0008_fragment_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='fragment',
            name='text',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='fragment',
            name='url',
            field=models.TextField(blank=True),
        ),
    ]
