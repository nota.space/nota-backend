# Generated by Django 3.0.8 on 2022-02-25 19:51

from django.db import migrations

def trust_old_userscripts(apps, schema_editor):

    # use the historical model
    Fragment = apps.get_model('rooms', 'Fragment')

    for row in Fragment.objects.all():
        # trust all old userscripts
        row.userscript_trusted = True

        # only update necessary fields
        row.save(update_fields=['userscript_trusted'])

class Migration(migrations.Migration):

    dependencies = [
        ('rooms', '0035_auto_20220217_1910'),
    ]

    operations = [
        # omit reverse_code, this trust is not reversible
        # migrations.RunPython(
            # trust_old_userscripts,
            # reverse_code=migrations.RunPython.noop),
    ]
