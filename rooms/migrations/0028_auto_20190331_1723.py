# Generated by Django 2.1.1 on 2019-03-31 15:23

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rooms', '0027_remove_fragment_censored'),
    ]

    operations = [
        migrations.RenameField(
            model_name='roomuser',
            old_name='delete',
            new_name='can_delete',
        ),
        migrations.RenameField(
            model_name='roomuser',
            old_name='read',
            new_name='can_read',
        ),
        migrations.RenameField(
            model_name='roomuser',
            old_name='write',
            new_name='can_write',
        ),
    ]
