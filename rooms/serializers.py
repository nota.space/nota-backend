import pprint
from itertools import chain

from django.utils import timezone
from django.shortcuts import get_object_or_404
from django.db import connection
from rest_framework import serializers
from guardian.shortcuts import get_perms

from uploads.models import File
from uploads.serializers import FileSerializer
from .models import Fragment, Room, FragmentFiles

pp = pprint.PrettyPrinter(indent=4)


def pretty(s):
    pp.pprint(s)


class FragmentListSerializer(serializers.ListSerializer):

    def update(self, fragments, validated_data):
        fragment_mapping = {fragment.id: fragment for fragment in fragments}
        data_mapping = {item['id']: item for item in validated_data}

        # created, room, files, file_hashes
        # TODO what is boxed
        # TODO what is size_is_stored
        write_fields = [
            'x', 'y', 'width', 'height', 'scale', 'rotation', 'content', 'room',
            'type', 'text', 'changed', 'floating_z', 'boxed', 'size_is_stored',
            'userscript',
            'relative_rotation_point_x', 'relative_rotation_point_y',
            'removed_from_space'
        ]

        # Perform creations and updates.
        ret = []
        for fragment_id, data in data_mapping.items():
            fragment = fragment_mapping.get(fragment_id, None)
            if fragment is None:
                pass
                #ret.append(self.child.create(data))
            else:
                #del data['room']
                #del data['created_by']
                #del data['files']
                # TODO it should not be neccessary to do this manually
                fragment.changed = timezone.now()
                for f in write_fields:
                    #fragment[f] = data[f]
                    val = data.get(f, None)
                    if val is not None and f != 'changed':
                        setattr(fragment, f, data[f])
                #ret.append(self.child.update(fragment, data))


        Fragment.objects.bulk_update(fragments, write_fields)

        # Perform deletions.
        #for fragment_id, fragment in fragment_mapping.items():
            #if fragment_id not in data_mapping:
                #fragment.delete()

        return ret

    def create(self, validated_data, room):
        create_data = []
        uploaded_files = {}

        user = self.context.get('request').user
        for index, fragment in enumerate(validated_data):
            if not user.is_anonymous:
                fragment['created_by'] = user
            if 'userscript' in fragment:
                fragment['userscript_author'] = user
                fragment['userscript_by_staff'] = user.is_staff
                fragment['userscript_trusted'] = user.is_staff

            if 'file_hashes' in fragment:
                file_hashes = fragment['file_hashes']
                if file_hashes:
                    uploaded_files[index] = file_hashes
                del fragment['file_hashes']

            create_data.append(fragment)

        fragments = [Fragment(**item) for item in create_data]
        # set room object, so it won't query the room table again.
        for fragment in fragments:
            fragment.room = room

        created_fragments = Fragment.objects.bulk_create(fragments)

        if not uploaded_files:
            return created_fragments

        # prefetch files: query files table (in 1 query), need to flatten the list here, because it's nested
        files = list(File.objects.filter(hash__in=set(chain.from_iterable(uploaded_files.values()))))
        # set Fragment Files, after we got the fragment id.
        fragment_files = []
        for index, fragment in enumerate(created_fragments):
            if index in uploaded_files:
                for file_hash in uploaded_files[index]:
                    fragment_file = FragmentFiles()
                    fragment_file.fragment = fragment
                    fragment_file.file = next(filter(lambda file: file.hash == file_hash, files))
                    fragment_files.append(fragment_file)
        FragmentFiles.objects.bulk_create(fragment_files)
        return created_fragments


class FragmentFilesSerializer(serializers.ModelSerializer):
    class Meta:
        model = FragmentFiles
        fields = ['fragment_id', 'file_id']


class FragmentSerializer(serializers.ModelSerializer):
    file_hashes = serializers.ListField(
        child=serializers.CharField(required=False),
        required=False
    )
    #room = serializers.IntegerField(write_only=True)
    files = FileSerializer(many=True, read_only=True)
    url = serializers.CharField(read_only=True)
    id = serializers.IntegerField(required=False)
    size_is_stored = serializers.BooleanField(required=False)

    class Meta:
        list_serializer_class = FragmentListSerializer
        model = Fragment
        fields = (
            'x', 'y', 'width', 'height', 'scale', 'rotation', 'content', 'room',
            'files', 'file_hashes',
            # should id be in here? does that mean it will be changed from user input?
            'type', 'text', 'id', 'changed', 'floating_z', 'boxed', 'url', 'size_is_stored',
            'relative_rotation_point_x', 'relative_rotation_point_y',
            'userscript',
            # readonly:
            'userscript_author', 'userscript_by_staff', 'userscript_trusted',
            'removed_from_space'
        )
        # these fields should not be automatically set from user input,
        # only from the request info, or directly in an a view
        read_only_fields = (
            'userscript_author',
            'userscript_by_staff',
            'userscript_trusted'
        )

    def to_representation(self, fragment):
        request = self.context.get("request")

        user = self.context.get('user', None)
        if user is not None:
            user = request.user
            # always and only execute scripts, if the fragment
            # was not updated, and created by current user or by a staff user
            # or it was updated by a staff user
            allow_script = False
            if fragment.updated_by is None:
                if fragment.created_by.is_staff or user == fragment.created_by:
                    allow_script = True
            elif fragment.updated_by.is_staff or user == fragment.updated_by:
                allow_script = True

        # backwards compatibility for old fragments
        # TODO actually migrate data....
        if fragment.userscript_author is None:
            fragment.userscript_author = fragment.created_by

        fragment.userscript_by_staff = fragment.userscript_author.is_staff


        ret = super(FragmentSerializer, self).to_representation(fragment)
        ret['created_by'] = fragment.created_by.username
        ret['created_by_staff'] = fragment.created_by.is_staff
        ret['created'] = fragment.created
        ret['changed'] = fragment.changed
        if fragment.updated_by is not None:
            ret['updated_by'] = fragment.updated_by.username
        if fragment.files is not None and fragment.files.count() > 0:
            url = fragment.files.all()[0].file.url
            ret['url'] = url
        ret['filehashes'] = []
        for f in fragment.files.all():
            ret['filehashes'].append(f.hash)

        return ret

    def update(self, instance, validated_data):
        request = self.context.get("request")
        user = request.user
        update_data = validated_data
        update_data['updated_by'] = user

        if 'userscript' in update_data:
            # do not accept the execute value coming from frontend
            # although it should be overwritten in to_representation, anyways
            update_data['userscript'].pop('execute', None)

            old_src = instance.userscript.get('scriptSourceString', None)
            new_src = update_data.get('userscript', {}).get('scriptSourceString', None)
            if  old_src != new_src:
                # userscript changed, set the new author from request user
                update_data['userscript_author'] = user
                update_data['userscript_by_staff'] = user.is_staff
                # this means we invalidate previously established trust on
                # updates of the user script.. do we want that?
                update_data['userscript_trusted'] = user.is_staff

        instance = super().update(instance, update_data)
        return instance

    def create(self, validated_data):
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
            create_data = validated_data

            if 'userscript' in create_data:
                create_data['userscript_author'] = user
                create_data['userscript_by_staff'] = user.is_staff
                create_data['userscript_trusted'] = user.is_staff

            # AnonymousUser cannot be saved
            if not user.is_anonymous:
                create_data['created_by'] = user
            files = None
            if(create_data.get('url') != ''):
                if create_data.get('file_hashes', None) is not None:
                    file_hashes = create_data.pop('file_hashes')
                    files = []
                    for file_hash in file_hashes:
                        f = get_object_or_404(
                            File, hash=file_hash
                        )
                        files.append(f)

            fragment = Fragment.objects.create(**create_data)
            if(files is not None):
                fragment.files.set(files)
                fragment.save()

            return fragment


class RoomSerializer(serializers.ModelSerializer):
    fragment_set = FragmentSerializer(
        many=True, read_only=True
    )
    username = serializers.SerializerMethodField()
    permissions = serializers.SerializerMethodField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_created_by(self, room):
        return room.created_by

    def get_permissions(self, room):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user

        if not user:
            if room.anonymous_access:
                return ['can_read']
            else:
                return []
        else:
            return [
                p for p in get_perms(user, room)
                if p in ['room_admin', 'modify_room_fragments', 'view_room_fragments']
            ]

    def get_username(self, room):
        return room.created_by.username

    def to_internal_value(self, data):
        if not data.get('name'):
            raise serializers.ValidationError({
                'name': 'This field is required.'
            })
        return data

    def create(self, validated_data):
        return Room.objects.create(**validated_data)

    class Meta:
        model = Room
        fields = ('id', 'name', 'anonymous_access', 'listed', 'grounded', 'fragment_set', 'created_by', 'username', 'permissions')

class SimpleRoomSerializer(RoomSerializer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        model = Room
        fields = ('id', 'name', 'anonymous_access', 'listed', 'grounded', 'created_by', 'username')

class PublicSimpleRoomSerializer(SimpleRoomSerializer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    class Meta:
        model = Room
        fields = ('id', 'name', 'username', 'grounded')


class FragmentScriptListSerializer(serializers.ListSerializer):

    def update(self, fragments, validated_data):
        fragment_mapping = {fragment.id: fragment for fragment in fragments}
        data_mapping = {item['id']: item for item in validated_data}

        write_fields = [
            'userscript'
        ]

        # Perform creations and updates.
        ret = []
        for fragment_id, data in data_mapping.items():
            fragment = fragment_mapping.get(fragment_id, None)
            if fragment is None:
                pass
            else:
                fragment.changed = timezone.now()
                for f in write_fields:
                    val = data.get(f, None)
                    if val is not None and f != 'changed':
                        setattr(fragment, f, data[f])

        print('start UPDATE, queries up to now {}'.format(len(connection.queries)))
        Fragment.objects.bulk_update(fragments, write_fields)
        print('finished UPDATE, queries up to now {}'.format(len(connection.queries)))
        # Perform deletions.
        #for fragment_id, fragment in fragment_mapping.items():
            #if fragment_id not in data_mapping:
                #fragment.delete()

        return ret

class FragmentScriptSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        list_serializer_class = FragmentScriptListSerializer
        model = Fragment
        fields = (
            'id',
            'userscript'
        )

    def update(self, instance, validated_data):
        request = self.context.get("request")
        instance = super().update(instance, validated_data)
        return instance
