from django.urls import path

from rooms.views import (
    SimpleRoomViewSet, RoomView, RoomByNameView, FragmentView, FragmentsView,
    RemoveFragmentFromRoomView, PublicRoomList, SetAccessView, SetAccessForRoomView,
    SearchAndReplaceScripts, SearchScripts, ListScriptLines, RoomPermissionsView,
    RoomGroupPermissionsView, SetOptionsView
)

room_list = SimpleRoomViewSet.as_view({
    'get': 'list',
})
# should not be used. also not secured.
#room_update = RoomViewset.as_view({
    #'put': 'update',
#})

urlpatterns = [
    path('list_script_lines/', ListScriptLines.as_view(),
        name="list_script_lines"),
    path('search_scripts/', SearchScripts.as_view(),
        name="search_scripts"),
    path('search_and_replace_scripts/', SearchAndReplaceScripts.as_view(),
        name="search_and_replace_scripts"),
    path('create/', RoomView.as_view(), name="room_create"),
    path('list/', room_list, name="room_list"),
    path('public_list/', PublicRoomList.as_view(), name="public_room_list"),
    #path('update/{id}', room_update, name="room_update"),
    #path('partial_update/{id}', room_partial_update, name="room_partial_update"),
    path('by_name/<str:user_name>/<str:room_name>/', RoomByNameView.as_view(), name="room_by_name"),
    path('fragments/', FragmentsView.as_view(), name="fragments"),
    path('fragment/', FragmentView.as_view(), name="create_fragment"),
    path('fragment/<int:pk>', FragmentView.as_view(), name="get_fragment"),
    path(
        'fragment/remove_from_room/',
        RemoveFragmentFromRoomView.as_view(), name="get_fragment"
    ),
    path('set_options/', SetOptionsView.as_view(), name="set_options"),
    path('set_access/', SetAccessView.as_view(), name="set_access"),
    path('set_access_for_room/', SetAccessForRoomView.as_view(), name="set_access_for_room"),
    path('permissions/<int:pk>', RoomPermissionsView.as_view(), name="room_permissions"),
    path('group_permissions/', RoomGroupPermissionsView.as_view(), name="room_group_permissions"),
]


