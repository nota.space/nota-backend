from django.contrib import admin
from django.utils.html import format_html
from guardian.admin import GuardedModelAdmin

from rooms.models import Fragment, Room


def titled_related_filter(title):
    class TitledFieldListFilter(admin.RelatedFieldListFilter):
        def __new__(cls, *args, **kwargs):
            instance = admin.RelatedFieldListFilter.create(*args, **kwargs)
            instance.title = title
            return instance
    return TitledFieldListFilter

def titled_field_filter(title):
    class TitledFieldListFilter(admin.FieldListFilter):
        def __new__(cls, *args, **kwargs):
            instance = admin.FieldListFilter.create(*args, **kwargs)
            instance.title = title
            return instance
    return TitledFieldListFilter

# With object permissions support
from uploads.models import File


class UserInlineAdmin(admin.TabularInline):
    model = Room.users.through


class FragmentInlineAdmin(admin.TabularInline):
    show_change_link = True
    model = Fragment
    fields = (
            'id',
            'type',
            'created_by',
            'updated_by',
            'url',
            'files',
            'created',
            'changed',
            'removed_from_space',
    )
    readonly_fields = (
            'id',
            'type',
            'created_by',
            'updated_by',
            'url',
            'files',
            'created',
            'changed',
    )

    def has_delete_permission(self, request, obj=None):
        return False


class RoomAdmin(GuardedModelAdmin):
    inlines = (UserInlineAdmin, FragmentInlineAdmin,)
    list_display = ('__str__', 'name', 'space_url', 'created_by',
            'number_of_fragments',
            'anonymous_access',
            'tag_list',
    )
    search_fields = [
            'name',
            'tags__name',
            'created_by__username',
    ]
    list_filter = [
            'anonymous_access',
            'active',
            ('fragment_set__created', titled_related_filter('fragments created')),
            ('fragment_set__changed', titled_related_filter('fragments changed')),
            ('tags__name', titled_related_filter('tags__name')),
            'created_by',
    ]

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related('tags')

    def tag_list(self, obj):
        return u", ".join(o.name for o in obj.tags.all())

    def number_of_fragments(self, instance):
        fragments = Fragment.objects.filter(room=instance)
        return fragments.count()

    def save_model(self, request, obj, form, change):
        if obj.id:
            super().save_model(request, obj, form, change)
        else:
            # allow setting created_by from Django Admin
            if obj.created_by is None:
                obj.created_by = request.user
            obj.save()

    def add_view(self, request, form_url='', extra_context=None):
        self.exclude = ('created_by')
        return super().add_view(
            request, form_url=form_url, extra_context=extra_context)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.exclude = ('created_by')
        return super().change_view(
            request, object_id, form_url=form_url, extra_context=extra_context)

    def space_url(self, room):
        return format_html(
                '<a href="/?user={user}&room={name}"' +
                    'onclick="' +
                    'javascript:event.target.port='+
                    'event.target.port==8000 ? '+
                    ' 8080 : event.target.port' +
                    '">' +
                    '{name} ({user})</a>',
                user=room.created_by,
                name=room.name
        )


class FragmentAdmin(GuardedModelAdmin):
    readonly_fields = ('files',)
    list_display = (
            '__str__',
            'room',
            'type',
            'url',
            'userscript_trusted',
            'userscript',
            'created',
            'changed',
            'content',
            'removed_from_space',
    )
    list_filter = [
            'removed_from_space',
            'created',
            'changed',
            'type',
            'userscript_trusted',
            ('created_by', titled_field_filter('fragment creator')),
            ('room__created_by', titled_related_filter('room creator')),
            ('room__name', titled_related_filter('room name')),
    ]

    def save_model(self, request, obj, form, change):
        if obj.id:
            super().save_model(request, obj, form, change)
        else:
            obj.created_by = request.user
            obj.save()

    def add_view(self, request, form_url='', extra_context=None):
        self.exclude = ('created_by',)
        return super().add_view(request, form_url=form_url, extra_context=extra_context)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.exclude = ()
        return super().change_view(request, object_id, form_url=form_url, extra_context=extra_context)


class RoomUserAdmin(GuardedModelAdmin):
    def save_model(self, request, obj, form, change):
        if obj.id:
            super().save_model(request, obj, form, change)
        else:
            obj.save()

    def add_view(self, request, form_url='', extra_context=None):
        self.exclude = ()
        return super().add_view(request, form_url=form_url, extra_context=extra_context)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        self.exclude = ()
        return super().change_view(request, object_id, form_url=form_url, extra_context=extra_context)


admin.site.register(Room, RoomAdmin)
admin.site.register(Fragment, FragmentAdmin)
