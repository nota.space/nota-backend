import pprint

from itertools import groupby

from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import Group, Permission
from django.utils import timezone
from django.db import transaction
from django.db.models import Q, Prefetch
from django.db import connection
from guardian.models import GroupObjectPermission
from guardian.shortcuts import (get_objects_for_user, get_objects_for_group,
    assign_perm, remove_perm, get_users_with_perms,
    get_groups_with_perms, get_group_perms, get_user_perms, get_perms_for_model)

from rest_framework.authentication import TokenAuthentication
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from rest_framework import status

from json import dumps

from rooms.models import Room, Fragment
from accounts.models import User
from rooms.serializers import PublicSimpleRoomSerializer, SimpleRoomSerializer, RoomSerializer, FragmentSerializer

pp = pprint.PrettyPrinter(indent=4)


def pretty(s):
    pp.pprint(s)


# This is a specialization of guardian.shortcuts.get_object_for_group.
# The option to overwrite GroupObjectPermission model is removed.
def get_rooms_for_group(group):
    app_label = 'rooms'
    codenames = set()

    perms = ['view_room_fragments', 'modify_room_fragments', 'room_admin']
    ctype = ContentType.objects.get_for_model(Room)

    # Compute codenames set and ctype if possible
    for perm in perms:
        codename = perm
        codenames.add(codename)

    queryset = Room.objects.all()

    # At this point, we should have both ctype and queryset and they should
    # match which means: ctype.model_class() == queryset.model
    # we should also have ``codenames`` list

    # Now we should extract list of pk values for which we would filter
    # queryset
    group_model = GroupObjectPermission
    groups_obj_perms_queryset = group_model.objects.filter(group=group, permission__content_type=ctype)
    if len(codenames):
        groups_obj_perms_queryset = groups_obj_perms_queryset.filter(
            permission__codename__in=codenames)
    fields = ['object_pk', 'permission__codename']

    groups_obj_perms = groups_obj_perms_queryset.values_list(*fields)
    data = list(groups_obj_perms)

    keyfunc = lambda t: t[0]  # sorting/grouping by pk (first in result tuple)
    data = sorted(data, key=keyfunc)
    pk_list = []
    perm_dict = {}
    for pk, group in groupby(data, keyfunc):
        obj_codenames = {e[1] for e in group}
        pk_list.append(pk)
        perm_dict[pk] = obj_codenames
    objects = queryset.filter(pk__in=pk_list).all()
    return {obj: perm_dict[str(obj.pk)] for obj in objects}


class PublicRoomList(APIView):
    """
    Returns a room, uniquely identified by the user and room name
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (AllowAny,)

    def dispatch(self, *args, **kwargs):
        response = super().dispatch(*args, **kwargs)
        # print('Queries counted: {}'.format(len(connection.queries)))
        return response

    def get(self, request):
        public_rooms = Room.objects.filter(Q(anonymous_access=True, listed=True))
        serializer = PublicSimpleRoomSerializer(public_rooms, context={'request': request}, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class RoomView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request):

        room_name = request.data["name"]
        anonymous_access = request.data.get("anonymous_access") or False
        create_data = {
            'name': room_name,
            'created_by': request.user,
            'anonymous_access': anonymous_access,
            'admin': request.user,
        }
        serializer = RoomSerializer(data=create_data)
        serializer.is_valid(raise_exception=True)
        room = serializer.save();

        assign_perm('room_admin', request.user, room)

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class SimpleRoomViewSet(ModelViewSet):
    serializer_class = SimpleRoomSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def dispatch(self, *args, **kwargs):
        response = super().dispatch(*args, **kwargs)
        # print('Queries counted: {}'.format(len(connection.queries)))
        return response

    def get_queryset(self):
        user = self.request.user
        perms = ['rooms.view_room_fragments', 'rooms.modify_room_fragments', 'rooms.room_admin']

        admin_only = self.request.query_params.get('admin_only')
        if admin_only:
            perms = ['rooms.room_admin']
            
        qs = get_objects_for_user(user, perms, any_perm=True, with_superuser=False)

        return qs.order_by('name')


class RoomGroupPermissionsView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        group_id = self.request.query_params.get('id')
        group = get_object_or_404(Group.objects, id=group_id)

        if not any(request.user.has_perm(perm, group) for perm in ['group_admin', 'group_user']):
            return Response("Not authorized", status=status.HTTP_403_FORBIDDEN)

        permits = get_rooms_for_group(group)

        data = [
            { 'space': SimpleRoomSerializer(room).data,
              'permissions': perms
            } for (room, perms) in permits.items()]
        return Response(data, status=status.HTTP_200_OK)

    @transaction.atomic
    def put(self, request):

        groupid = request.data.get('groupid')
        if not groupid:
            return Response("No result", status=status.HTTP_404_NOT_FOUND)

        group = get_object_or_404(Group.objects, id=groupid)
        if not request.user.has_perm('group_admin', group):
            raise ValidationError("Not admin of group!")

        perm = None
        perm_code = request.data.get('perm_code')

        if perm_code != 'none':
            try:
                perm = get_perms_for_model(Room).get(codename=perm_code)
            except Permission.DoesNotExist:
                raise ValidationError("Permission not found!")

        roomids = request.data.get('spaceids')
        if perm_code == 'none':
            for roomid in roomids:
                room = get_object_or_404(Room, pk=roomid)

                perms = get_group_perms(group, room)
                if not perms:
                    raise ValidationError("Group not assigned to space!")

                for perm in perms:
                    remove_perm(perm, group, room)

        else:
            for roomid in roomids:
                room = get_object_or_404(Room, pk=roomid)

                if not request.user.has_perm('room_admin', room):
                    raise ValidationError("Not admin of room!")

                perms = get_group_perms(group, room)
                if perms:
                    raise ValidationError("Group already assigned to space!")

                assign_perm(perm, group, room)

        return Response('OK', status=status.HTTP_200_OK)


class RoomPermissionsView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, pk):
        room = Room.objects.get(pk=pk)

        if not request.user.has_perm('room_admin', room):
            return Response("Not authorized", status=status.HTTP_403_FORBIDDEN)

        after = request.query_params.get('after')
        of = request.query_params.get('of')

        if of == 'users':
            users = get_users_with_perms(room, with_group_users=False).order_by('id')

            if after and after.isnumeric() and after != '0':
                users = users.filter(id__gt=after)
            r = [{
                    'id': user.id,
                    'username': user.username,
                    'permissions': get_user_perms(user, room),
                } for user in users[:40]]
            return Response(r, status=status.HTTP_200_OK)
        elif of == 'groups':
            groups = get_groups_with_perms(room).order_by('id')
            if after and after.isnumeric and after != '0':
                groups = groups.filter(id__gt=after)
            r = [{
                    'id': group.id,
                    'groupname': group.name,
                    'permissions': get_group_perms(group, room),
                } for group in groups[:40]]
            return Response(r, status=status.HTTP_200_OK)
        else:
            return Response("No result", status=status.HTTP_404_NOT_FOUND)

    @transaction.atomic
    def put(self, request, pk):
        room = Room.objects.get(pk=pk)

        if not request.user.has_perm('room_admin', room):
            return Response("Not authorized", status=status.HTTP_403_FORBIDDEN)
        print

        of = request.data.get('of')
        ids = request.data.get('ids')

        if not ids or len(ids) > 20:
            raise ValidationError("Need reasonable ids.")

        perm_code = request.data.get('perm_code')
        new_perm = None
        if perm_code != "none":
            try:
                new_perm = get_perms_for_model(Room).get(codename=perm_code)
            except Permission.DoesNotExist:
                raise ValidationError("Permission not found!")


        if of == 'user':
            for k in ids:
                user = get_object_or_404(User.objects, id=k)

                if user.id == request.user.id:
                    raise ValidationError("Can't change your own permissions!")

                if user == room.created_by:
                    raise ValidationError("Can't change permission of owner!")

                perms = get_user_perms(user, room)
                for perm in perms:
                    remove_perm(perm, user, room)

                # indicating no replacement
                if perm_code != "none":
                    assign_perm(new_perm, user, room)

            return Response('OK', status=status.HTTP_200_OK)
        elif of == 'group':
            for k in ids:
                group = get_object_or_404(Group, id=k)

                perms = get_group_perms(group, room)

                for perm in perms:
                    remove_perm(perm, group, room)

                # indicating no replacement
                if perm_code != "none":
                    assign_perm(new_perm, group, room)

                if not request.user.has_perm('room_admin', room):
                    raise ValidationError("Can't drop your own admin privileges!")

            return Response('OK', status=status.HTTP_200_OK)
        else:
            raise ValidationError("Need to specify user or group to change permission of!")

class SetOptionsView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        room_id = request.data.get("room_id")
        room = get_object_or_404(Room.objects, pk=room_id)

        if not request.user.has_perm('room_admin', room):
            return Response("Not authorized", status=status.HTTP_403_FORBIDDEN)

        anonymous_access = request.data.get("anonymous_access")
        listed = request.data.get("listed")
        grounded = request.data.get("grounded")

        room.anonymous_access = anonymous_access
        room.listed = listed
        room.grounded = grounded
        room.save()
        return Response("Ok", status=status.HTTP_200_OK)

class SetAccessView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        room_id = request.data.get("room_id")
        target_type = request.data.get("target_type")
        target_id = request.data.get("target")
        can_read = request.data.get("can_read") or False
        can_modify = request.data.get("can_modify") or False
        can_manage = request.data.get("can_manage") or False

        room = Room.objects.get(pk=room_id)
        if not request.user.has_perm('room_admin', room):
            return Response("Not authorized", status=status.HTTP_403_FORBIDDEN)
        target = Group.objects.get(pk=target_id) if target_type == 'group' else User.objects.get(pk=target_id)
        if can_manage:
            assign_perm('room_admin', target, room)
        elif can_modify:
            assign_perm('modify_room_fragments', target, room)
        elif can_read:
            assign_perm('view_room_fragments', target, room)
        return Response("Authorized", status=status.HTTP_200_OK)


# For compatibility with old api until a better user interface.
class SetAccessForRoomView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):

        data = request.data
        user_name = data["user_name"]
        room_name = data["room_name"]
        users_to_add = data["users_to_add"]
        can_read = data.get("can_read") or False
        can_write = data.get("can_write") or False
        can_delete = data.get("can_delete") or False

        room_user = User.objects.get(username=user_name)
        room = Room.objects.filter(
            name=room_name, created_by=room_user
        ).first()

        if room is None:
            return Response("OK", status=status.HTTP_200_OK)

        if not request.user.has_perm('room_admin', room):
            return Response("Not authorized", status=status.HTTP_403_FORBIDDEN)

        for user_to_add_name in users_to_add:
            target = User.objects.all().filter(username=user_to_add_name).first()
            if target is not None:
                if can_write or can_delete:
                    assign_perm('modify_room_fragments', target, room)
                elif can_read:
                    assign_perm('view_room_fragments', target, room)


        return Response("Authorized", status=status.HTTP_200_OK)


class RoomByNameView(APIView):
    """
    Returns a room, uniquely identified by the user and room name
    """
    serializer_class = RoomSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (AllowAny,)

    ##permission_classes = (IsAuthenticated,)

    def dispatch(self, *args, **kwargs):
        response = super().dispatch(*args, **kwargs)
        # print('Queries counted: {}'.format(len(connection.queries)))
        return response

    def get_queryset(self):
        return Room.objects.prefetch_related('fragment_set__files').filter(active=True)

    def get(self, request, user_name, room_name, format=None):
        """
        Returns Room Data
        """
        print('0000000000000========')
        room_user = User.objects.get(username=user_name)
        # gets room. also checks if a room exists with room_name and room_user
        # if not creates Response with 404
        # room = get_object_or_404(Room.objects, name=room_name, created_by=room_user)
        qs = Room.objects.prefetch_related(
            Prefetch("fragment_set", queryset=Fragment.objects.filter(removed_from_space=False)),
            "fragment_set__files",
            "fragment_set__created_by",
            "fragment_set__files"
        ).filter(name=room_name, created_by=room_user).first()

        user = request.user

        if qs is None:
            return Response("No result", status=status.HTTP_404_NOT_FOUND)
        elif qs.can_view_fragments(user):
            serializer = RoomSerializer(qs, context={
                'request': request,
                'user': user
            })
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response("Not authorized", status=status.HTTP_403_FORBIDDEN)


'''
set the room associated with the given fragment id to None
i.e. remove the fragment from the room
'''
class RemoveFragmentFromRoomView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def dispatch(self, *args, **kwargs):
        response = super().dispatch(*args, **kwargs)
        # print('Queries counted: {}'.format(len(connection.queries)))
        return response

    @transaction.atomic
    def delete(self, request):
        # get space that fragments should be removed from
        space_id = request.data['space_id']
        room = Room.objects.get(id=space_id)
        # make sure requesting user is allowed to remove from room
        if not room.can_modify_fragments(request.user):
            return Response(status=status.HTTP_403_FORBIDDEN)
        # get fragments to be deleted, make sure they are really in the space
        # claimed by the request
        frag_ids = list(map(lambda f: f['id'], request.data['fragments']))

        # bulk update:
        Fragment.objects.filter(id__in=frag_ids).filter(room=space_id).update(
                removed_from_space=True)

        # print('Queries counted: {}'.format(len(connection.queries)))
        return Response(status=status.HTTP_200_OK)


class FragmentsView(APIView):
    """
    Batch API Endpoint for Fragments
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def dispatch(self, *args, **kwargs):
        response = super().dispatch(*args, **kwargs)
        # print('Queries counted: {}'.format(len(connection.queries)))
        return response

    def get_queryset(self):
        return Fragment.objects

    def get(self, request, format=None):
        """
        Get Fragment by id and check if the request.user is a member of room.users
        """
        ids = request.GET.getlist('fragment_ids')[0]
        if ids == '':
            return Response({
                "fragments": []
            })
        else:
            ids = ids.split(',')
            room_id = request.GET.get('room_id')
            qs = Fragment.objects.prefetch_related(
                'files',
                'created_by',
            ).filter(removed_from_space=False, room=room_id, id__in=ids)
            serializer = FragmentSerializer(qs.all(), many=True)

            return Response({
                "fragments": serializer.data
            })

    # bad performance? TODO
    @transaction.atomic
    def put(self, request):
        room_id = request.data['room']
        fragments = list(request.data['fragments'])

        # do not allow setting the space from frontend
        for fragment in fragments:
            if 'room' in fragment:
                del fragment['room']

        # check space permissions for user
        room = Room.objects.get(pk=room_id)
        if not room.can_modify_fragments(request.user):
            return Response(status=status.HTTP_403_FORBIDDEN)
        frag_ids = list(map(lambda f: f['id'], fragments))
        # get fragment objects to be updated
        # also make sure they are in the space claimed by the request
        fragment_objects = Fragment.objects.filter(pk__in=frag_ids).filter(room=room)

        serializer = FragmentSerializer(
            data=fragments,
            context={'request': request},
            many=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.update(fragment_objects, serializer.validated_data)

        # print('Queries counted: {}'.format(len(connection.queries)))
        return Response(status=status.HTTP_200_OK)

    def post(self, request):
        room_id = request.data['room']
        fragments = list(request.data['fragments'])

        # check space permissions for user
        room = Room.objects.get(pk=room_id)
        if not room.can_modify_fragments(request.user):
            return Response(status=status.HTTP_403_FORBIDDEN)

        serializer = FragmentSerializer(
            data=fragments,
            context={'request': request},
            many=True
        )
        serializer.is_valid(raise_exception=True)
        resp = serializer.create(serializer.validated_data, room)
        # print('Queries counted: {}'.format(len(connection.queries)))
        # print(connection.queries)
        return Response(list(map(lambda f: f.id, resp)), status=status.HTTP_201_CREATED)


class FragmentView(APIView):
    """
    API Endpoint for Fragments
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def dispatch(self, *args, **kwargs):
        response = super().dispatch(*args, **kwargs)
        # print('Queries counted: {}'.format(len(connection.queries)))
        return response

    def get_queryset(self):
        return Fragment.objects

    def get(self, request, pk, format=None):
        """
        Get Fragment by id and check if the request.user is a member of room.users
        """
        fragment = get_object_or_404(Fragment, id=pk, room__users__id=request.user.id)
        serializer = FragmentSerializer(fragment)
        return Response(serializer.data)

    def put(self, request, pk):
        serializer = FragmentSerializer(data=request.data, context={'request': request})
        if serializer.is_valid():
            fragment = Fragment.objects.filter(id=pk).first()
            room_obj = serializer.validated_data.get('room', None)
            if room_obj:
                # room_obj = get_object_or_404(Room, id=room_id)
                if room_obj.can_modify_fragments(request.user):
                    data = serializer.validated_data
                    data['room'] = room_obj
                    fragment.changed = timezone.now()
                    serializer.update(fragment, data)
                    return Response(status=status.HTTP_204_NO_CONTENT)
                else:
                    return Response(status=status.HTTP_403_FORBIDDEN)

        # print('Queries counted: {}'.format(len(connection.queries)))
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):
        serializer = FragmentSerializer(data=request.data, context={'request': request})
        # pretty(request.data)
        if serializer.is_valid():
            room_obj = serializer.validated_data.get('room', None)
            if room_obj:
                # room_obj = get_object_or_404(Room, id=room_id)
                if room_obj.can_modify_fragments(request.user):
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_201_CREATED)
                else:
                    return Response(status=status.HTTP_403_FORBIDDEN)
        else:
            # print("INVALID REQUEST DATA:")
            pretty(serializer.errors)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SearchAndReplaceScripts(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    @transaction.atomic
    def post(self, request):
        user = request.user
        data = request.data
        search = data.get("search") or None
        replace = data.get("replace") or None

        print('replacing:')
        print('-----------------------------------------------')
        print(str(search))
        print('-----------------------------------------------')
        print('')
        print(' by ')
        print('')
        print('-----------------------------------------------')
        print(str(replace))
        print('-----------------------------------------------')

        if search is None or search == '':
            return Response("search needs to be set in request data",
                            status=status.HTTP_400_BAD_REQUEST)

        if not user.is_staff:
            return Response(status=status.HTTP_403_FORBIDDEN)

        if replace is None:
            replace = ''

        fragments = Fragment.objects.prefetch_related(
            'room',
        ).exclude(room=None)

        exclude_ids = []
        update_ids = []
        scripts = []

        for frag in fragments:
            scriptobj = frag.userscript
            script = ''
            altered_script = ''
            if scriptobj is not None:
                script = scriptobj.get('scriptSourceString', '')
                if script.find(search) != -1:
                    frag_room = frag.room
                    info = {}
                    info['space'] = str(frag_room)
                    info['script'] = script
                    scripts.append(info)
                    altered_script = script.replace(search, replace)
                    scriptobj['scriptSourceString'] = altered_script
                    frag.userscript = scriptobj
                    update_ids.append(frag.id)
                else:
                    exclude_ids.append(frag.id)

        print('total fragments: ' + str(len(update_ids) + len(exclude_ids)))
        print('updating fragments: ' + str(len(update_ids)))

        for frag in fragments:
            if frag.id in update_ids:
                frag.save(update_fields=['userscript'])

        # return Response('Fragments updated: ' + str(len(update_ids)), status=status.HTTP_200_OK)
        return Response(dumps(scripts), status=status.HTTP_200_OK)


class SearchScripts(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        user = request.user
        data = request.data

        search = request.GET.get('search')

        print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
        print('searching:')
        print('-----------------------------------------------')
        print(str(search))
        print('-----------------------------------------------')

        if search is None or search == '':
            return Response("search needs to be set in request data",
                            status=status.HTTP_400_BAD_REQUEST)

        if not user.is_staff:
            return Response(status=status.HTTP_403_FORBIDDEN)

        fragments = Fragment.objects.prefetch_related(
            'room',
            'room__created_by',
        ).exclude(room=None)

        fragment_by_room = {}
        rooms = set()
        scripts = {}

        num = 0

        for frag in fragments:
            scriptobj = frag.userscript
            script = ''
            if scriptobj is not None:
                script = scriptobj.get('scriptSourceString', '')
                if script.find(search) != -1:
                    frag_id = frag.id
                    frag_room = frag.room
                    rooms.add(frag_room)
                    if scripts.get(script) is None:
                        scripts[script] = set()
                    scripts[script].add(str(frag_room))

        for script in scripts:
            scripts[script] = list(scripts[script])

        print('Found ' + str(len(scripts)) + ' scripts')
        print('Queries counted: {}'.format(len(connection.queries)))
        return Response(dumps(scripts), status=status.HTTP_200_OK)


class ListScriptLines(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        user = request.user
        data = request.data

        print('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
        print('getting lines...')

        if not user.is_staff:
            return Response(status=status.HTTP_403_FORBIDDEN)

        fragments = Fragment.objects.exclude(room=None)

        lines = set()
        num = 0

        search_terms = [
            # 'modules.', 'fragment..', 'this.'
            'pos.',
            'sel.',
            'mes.',
            'p.',
            '= modules.messages;',
            '= modules.messages',
            '=modules.messages',
        ]

        for frag in fragments:
            scriptobj = frag.userscript
            script = ''
            if scriptobj is not None:
                script = scriptobj.get('scriptSourceString', '')
                script_lines = script.splitlines()
                for line in script_lines:
                    add_line = False
                    for term in search_terms:
                        if line.find(term) != -1:
                            # add line, stop searching terms
                            lines.add(line)
                            num += 1
                            break

        lines = list(lines)

        print('Found ' + str(num) + ' lines')
        print('Queries counted: {}'.format(len(connection.queries)))
        return Response(dumps(lines), status=status.HTTP_200_OK)
