from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType
from django.db.models import JSONField
from django.db import models
from guardian.shortcuts import get_perms

from accounts.models import User
from django.core.exceptions import ValidationError
from taggit.managers import TaggableManager
from uploads.models import File


class RoomUser(models.Model):
    room = models.ForeignKey('Room', on_delete=models.PROTECT, null=False)
    user = models.ForeignKey(User, on_delete=models.PROTECT, null=False)
    can_read = models.BooleanField(null=False)
    can_write = models.BooleanField(null=False)
    can_delete = models.BooleanField(null=False)

    def __str__(self):
        return self.user.__str__() + ' -- ' + self.room.__str__()


class Room(models.Model):
    name = models.CharField(max_length=60, blank=False)
    created_by = models.ForeignKey(
        User, on_delete=models.PROTECT,
        related_name='created_rooms',
    )
    admin = models.ForeignKey(
        User, on_delete=models.PROTECT, related_name='admin',
        null=True,
    )
    users = models.ManyToManyField(
        User,
        through='RoomUser',
        through_fields=('room', 'user'),
    )
    anonymous_access = models.BooleanField(null=False)
    listed = models.BooleanField(null=False, default=True)
    grounded = models.BooleanField(null=False, default=False)
    active = models.BooleanField(null=False, default=True)
    tags = TaggableManager()


    class Meta:
        verbose_name = "Room"
        verbose_name_plural = "Rooms"
        unique_together = ("created_by", "name")
        default_permissions = ()
        permissions = [
            ("room_admin", "Can manage the space"),
            ("modify_room_fragments", "Can modify fragments of the space"),
            ("view_room_fragments", "Can view fragments of the space"),
        ]

    def __str__(self):
        return self.name + ' (' + self.created_by.__str__() + ')';

    def can_view_fragments(self, user):
        if self.anonymous_access:
            return True
        return any(x in ["room_admin", "modify_room_fragments", "view_room_fragments"] for x in get_perms(user, self))

    def can_modify_fragments(self, user):
        return any(x in ["room_admin", "modify_room_fragments"] for x in get_perms(user, self))

    def save(self, *args, **kwargs):
        self.validate_unique()
        super().save(*args, **kwargs)  # Call the "real" save() method.

    def validate_unique(self, exclude=None):
        try:
            super(Room, self).validate_unique(exclude)
        except ValidationError as e:
            raise e


# permissions are established by the combination of
# Fragment, Room, RoomUser, User
# where User is from request
# The fragment can be viewed, if the room is public, or if
# the RoomUser has can_read
class Fragment(models.Model):
    # The room can be set, if the RoomUser has can_write
    # The room can be removed, if the RoomUser has can_delete
    # RoomUser.can_edit / .can_write / .can_delete
    room = models.ForeignKey(Room, on_delete=models.PROTECT, null=True, related_name="fragment_set")
    removed_from_space = models.BooleanField(default=False, null=False)
    # These fields should be set based on the user doing a request
    created_by = models.ForeignKey(User, on_delete=models.PROTECT, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.PROTECT, null=True, related_name='updated_by')
    # created and changed should be set automatically
    created = models.DateTimeField(auto_now_add=True)
    changed = models.DateTimeField(auto_now_add=True)
    # These fields require the permission RoomUser.can_edit to change
    x = models.FloatField(null=False)
    y = models.FloatField(null=False)
    relative_rotation_point_x = models.FloatField(null=False, default=0.5)
    relative_rotation_point_y = models.FloatField(null=False, default=0.5)
    width = models.FloatField(null=False)
    height = models.FloatField(null=False)
    scale = models.FloatField(null=False)
    rotation = models.FloatField(null=False, default=0.0)
    content = JSONField(null=False)
    type = models.CharField(max_length=60, blank=False)
    url = models.TextField(blank=True)
    text = models.TextField(blank=True)
    files = models.ManyToManyField(
        File,
    )
    floating_z = models.FloatField()
    boxed = models.BooleanField(default=False, null=False)
    size_is_stored = models.BooleanField(default=False, null=False)

    # userscript
    userscript = JSONField(default=dict)
    # userscript_author should be set from the user doing the request, but only,
    # if the userscript is changed
    userscript_author = models.ForeignKey(User, on_delete=models.PROTECT, null=True, related_name='userscript_author')
    # userscript_by_staff should be set to True if the latest userscript edit
    # was done by a staff user
    userscript_by_staff = models.BooleanField(default=False, null=False)
    userscript_trusted = models.BooleanField(default=False, null=False)

    class Meta:
        verbose_name = "Fragment"
        verbose_name_plural = "Fragments"
        ordering = ('-floating_z',)

    def __str__(self):
        return f'Fragment ({self.x},{self.y}) {self.room}'


# this model is used to manage the fragment files table
class FragmentFiles(models.Model):
    fragment = models.ForeignKey(Fragment, on_delete=models.PROTECT, null=False)
    file = models.ForeignKey(File, on_delete=models.PROTECT, null=False)

    class Meta:
        db_table = 'rooms_fragment_files'
        managed = False

    def __str__(self):
        return self.fragment.__str__() + ' -- ' + self.file.__str__()
