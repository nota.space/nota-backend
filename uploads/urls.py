from django.urls import path

from uploads.views import FileUploadView, TestUploadView


urlpatterns = [
    path('test_upload/', TestUploadView.as_view()),
    path('upload/<filename>', FileUploadView.as_view()),
]
