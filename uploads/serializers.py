from rest_framework import serializers

from uploads.models import File


class FileSerializer(serializers.ModelSerializer):

    class Meta:
        model = File
        fields = ('file', 'type', 'hash', 'authorized')
