import os

from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.safestring import mark_safe

from accounts.models import User
from uploads.utils import sha256_hash
from random import random
from uploads.validators import VALID_AUDIO_MIMETYPES, VALID_IMAGE_MIMETYPES, VALID_VIDEO_MIMETYPES


def create_upload_to(instance, filename):
    # to defend from bruteforce filename guessing we create a hash prefix
    create_datetime = timezone.now()
    create_datetime_str = create_datetime.strftime('%Y%m%d%H%M%S')
    to_hash = '{}{}{}{}'.format(
        random(), settings.FILE_SALT, filename, create_datetime_str
    )
    hash_str = sha256_hash(to_hash)
    filepath = os.path.join(
        settings.UPLOAD_URL,
        str(create_datetime.year),
        str(create_datetime.month),
        str(create_datetime.day),
        hash_str[0:32],
        filename
    )
    return filepath


def get_file_type_by_mime(mimetype):
    if mimetype in VALID_AUDIO_MIMETYPES:
        return File.AUDIO
    elif mimetype in VALID_IMAGE_MIMETYPES:
        return File.IMAGE
    elif mimetype in VALID_VIDEO_MIMETYPES:
        return File.VIDEO
    else:
        return File.UNKNOWN


class FileManager(models.Manager):
    def create_file(self, file, user, authorized=False):
        file_mime_type = file.content_type
        file_type = get_file_type_by_mime(file_mime_type)
        file_obj = self.create(file=file, type=file_type, created_by=user, authorized=authorized)
        # this hash is used to identify a file on the client side without to reveal the id
        file_obj.create_hash()
        return file_obj

    def get_file_by_hash(self, hash_str):
        return self.get(hash=hash_str)


class File(models.Model):
    AUDIO = 'AU'
    IMAGE = 'IM'
    TEXT = 'TX'
    VIDEO = 'VI'
    UNKNOWN = 'UN'
    FILE_TYPE_CHOICES = (
        (AUDIO, 'Audio'),
        (IMAGE, 'Image'),
        (TEXT, 'Text'),
        (VIDEO, 'Video'),
        (UNKNOWN, 'Unknown'),
    )

    file = models.FileField(upload_to=create_upload_to, null=False)
    type = models.CharField(max_length=2, choices=FILE_TYPE_CHOICES, null=True)
    hash = models.CharField(max_length=64, null=True, unique=True)
    authorized = models.BooleanField(default=False, null=False)
    created_by = models.ForeignKey(User, on_delete=models.PROTECT, null=True)

    objects = FileManager()

    class Meta:
        verbose_name = "File"
        verbose_name_plural = "Files"

    def __str__(self):
        return self.file.url

    def create_hash(self):
        self.hash = sha256_hash('{}{}'.format(self.id, settings.FILE_SALT))
        self.save()

    def file_tag(self):
        if self.type == self.IMAGE:
            return mark_safe('<img src="%s" width="300" />' % self.file.url)
    file_tag.short_description = 'File'
