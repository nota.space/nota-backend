from django.contrib import admin
from django.contrib.admin import ModelAdmin

from uploads.models import File


def apply_authorization(modeladmin, request, queryset):
    for file in queryset:
        file.authorized = True
        file.save()


apply_authorization.short_description = 'Authorize selected files'


class FileInlineAdmin(admin.TabularInline):
    model=File
    fields = ('show_url',)
    def show_url(self, instance):
        return instance.file.url

class FileAdmin(ModelAdmin):
    list_display = ['file', 'created_by', 'authorized']
    list_filter = (
        ('created_by', admin.RelatedOnlyFieldListFilter),
    )
    fields = ['file_tag', 'file', 'type', 'fragment', 'created_by', 'authorized']
    readonly_fields = ['file_tag', 'file', 'type', 'fragment', 'created_by']
    actions = [apply_authorization]


admin.site.register(File, FileAdmin)
