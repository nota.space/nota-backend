import os
from django.core.exceptions import ValidationError


VALID_APPLICATION_MIMETYPES = (
    'application/pdf',
)
VALID_AUDIO_MIMETYPES = (
    'audio/midi',
    'audio/mpeg',
    'audio/webm',
    'audio/ogg',
    'audio/wav',
    'audio/mp3',
)
VALID_IMAGE_MIMETYPES = (
    'image/gif',
    'image/png',
    'image/jpeg',
    'image/webp',
    'image/bmp',
)
VALID_VIDEO_MIMETYPES = (
    'video/webm',
    'video/ogg',
    'video/mp4',
    'video/quicktime',
)

VALID_MIMETYPES_ALL = (
    VALID_APPLICATION_MIMETYPES + VALID_AUDIO_MIMETYPES + VALID_IMAGE_MIMETYPES + VALID_VIDEO_MIMETYPES
)

VALID_PDF_EXTENSIONS = (
    '.pdf',
)

VALID_AUDIO_EXTENSIONS = (
    '.mid',
    '.mpeg',
    '.mpg',
    '.wav',
    '.mp3',
    '.opus',
)

VALID_IMAGE_EXTENSIONS = (
    '.gif',
    '.png',
    '.jpg',
    '.webp',
    '.jpeg',
    '.bmp',
)

VALID_VIDEO_EXTENSIONS = (
    '.webm',
    '.ogg',
    '.mp4',
    '.m4v',
    '.mov',
)

VALID_FILE_EXTENSIONS_ALL = (
    VALID_PDF_EXTENSIONS + VALID_AUDIO_EXTENSIONS + VALID_IMAGE_EXTENSIONS + VALID_VIDEO_EXTENSIONS + ('',)
)


def validate_mimetype(file, valid_mimetypes):
    file_mime_type = file.content_type
    if file_mime_type not in valid_mimetypes:
        raise ValidationError('Unsupported file type: {}'.format(str(file_mime_type)[:50]))


def validate_fileextension(file, valid_file_extensions):
    ext = os.path.splitext(file.name)[1]
    if ext.lower() not in valid_file_extensions:
        raise ValidationError('Unacceptable file extension: {}'.format(str(ext.lower())[:50]))
