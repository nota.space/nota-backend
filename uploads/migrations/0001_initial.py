# Generated by Django 2.1.1 on 2018-11-20 19:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('rooms', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='File',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('file', models.FileField(upload_to='uploads/%Y/%m/%d/')),
                ('type', models.CharField(choices=[('AU', 'Audio'), ('IM', 'Image'), ('TX', 'Text'), ('VI', 'Video')], max_length=2, null=True)),
                ('fragment', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='rooms.Fragment')),
            ],
        ),
    ]
