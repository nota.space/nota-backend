import os

from django.core.exceptions import ValidationError
from django.views.generic import TemplateView
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.parsers import FileUploadParser
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from uploads.models import File
from uploads.validators import validate_mimetype, validate_fileextension, VALID_MIMETYPES_ALL, \
    VALID_FILE_EXTENSIONS_ALL, VALID_IMAGE_MIMETYPES, VALID_IMAGE_EXTENSIONS

from PIL import Image, ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True


class FileUploadView(APIView):
    parser_classes = (FileUploadParser,)
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = File.objects.all()

    def put(self, request, filename, format=None):
        file_obj = request.data.get('file', None)
        if request.user and file_obj:
            try:
                #validate_mimetype(file_obj, VALID_MIMETYPES_ALL)
                #validate_fileextension(file_obj, VALID_FILE_EXTENSIONS_ALL)
                file = File.objects.create_file(file=file_obj, user=request.user, authorized=True)

                if file.type == file.IMAGE:
                    # Creating resolutions...
                    dirname = os.path.dirname(file.file.path)
                    filename = os.path.basename(file.file.path)
                    im = Image.open(file.file.path)
                    width = im.size[0]
                    height = im.size[1]
                    max_im_size = max(im.size)
                    print("creating smaller image versions...")
                    for max_size in [4000, 2000, 1000, 500, 250, 125]:
                        #if(max_im_size < max_size):
                            #continue
                        print(max_size)
                        size = (max_size, max_size)
                        im.thumbnail(size, resample=Image.BICUBIC)
                        im.save(os.path.join(dirname, str(max_size) + "." + filename))

                    return Response(
                        status=200,
                        data={
                            "hash": file.hash,
                            "url": file.file.url,
                            "width": width,
                            "height": height
                        }
                    )
                else:
                    return Response(
                        status=200,
                        data={
                            "hash": file.hash,
                            "url": file.file.url,
                        }
                    )
            except ValidationError as ex:
                return Response(status=400, data=ex.message)
        else:
            return Response(status=400)


class TestUploadView(TemplateView):
    template_name = 'uploads/test_upload.html'
