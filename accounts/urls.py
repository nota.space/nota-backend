from django.urls import path

from accounts.views import SignUp, Login, TestAPIView, ChangePassword

urlpatterns = [
    path('create/', SignUp.as_view(), name="user_create"),
    path('login/', Login.as_view(), name="user_login"),
    path('test/', TestAPIView.as_view(), name="user_test"),
    path('change_password/', ChangePassword.as_view(), name='change_password'),
]
