from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from django.contrib.auth.hashers import check_password

from accounts.models import User
from accounts.serializers import UserRegisterSerializer

class ChangePassword(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all()

    def post(self, request, *args, **kwargs):
        request_user = request.user
        data = request.data
        current_password = data.get('current_password', None)
        new_password = data.get('new_password', None)

        if current_password is not None and new_password is not None:
            user = User.objects.get(id=request_user.id)
            if check_password(current_password, user.password):
                user.set_password(new_password)
                user.save()
                return Response('Password changed', status=status.HTTP_200_OK)
            else:
                return Response('Wrong current password',
                        status=status.HTTP_403_FORBIDDEN)

        else:
            return Response('password is missing in request',
                    status=status.HTTP_400_BAD_REQUEST)

    def get(self, request, format=None):
        return Response('change_password')

class SignUp(CreateAPIView):
    permission_classes = (AllowAny,)
    queryset = User.objects.all()
    serializer_class = UserRegisterSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        #authenticate(username=serializer.data['username'], password=serializer.data['password'])
        token = Token.objects.get(user__username=serializer.data['username'])
        serializer.validated_data['token'] = token.key
        return Response(serializer.validated_data, status=status.HTTP_201_CREATED, headers=headers)


class Login(ObtainAuthToken):
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'id': user.pk,
            'username': user.username,
            'is_staff': user.is_staff
        })


class TestAPIView(APIView):
    """
    Test
    """
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        """
        Returns Test Data
        """
        test = ['a', 'b', 'c', 1, 2, 3]
        return Response(test)
