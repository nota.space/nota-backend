from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from guardian.admin import GuardedModelAdmin
from rest_framework.authtoken.admin import TokenAdmin

from .models import User


# With object permissions support
class UserAdmin(UserAdmin):
    pass

admin.site.register(User, UserAdmin)
