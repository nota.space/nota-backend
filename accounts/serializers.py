from rest_framework.serializers import ModelSerializer

from accounts.models import User


class UserRegisterSerializer(ModelSerializer):

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)

    class Meta:
        model = User
        fields = ("first_name", "last_name", "email", "username", "password")
