from django.contrib.auth.models import AbstractUser, AnonymousUser
from django.db import models
from rest_framework.authtoken.models import Token


class AnonUser(AnonymousUser):
    pass


class User(AbstractUser):
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    modified_at = models.DateTimeField(auto_now=True, null=True)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        token, created = Token.objects.get_or_create(user=self)
