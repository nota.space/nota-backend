#!/bin/bash
if [ $# -lt 2 ]
then
    echo "Provide source filename and database target parameter"
else
    echo "Restoring from '$1' to '$2'"
    psql -U postgres $2 < $1
    #python manage.py loaddata $1
fi

