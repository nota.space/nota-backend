#!/bin/bash
# abort on failure
set -e
# TODO: remember steps taken


# exit if wrong dir
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
PWD=$(pwd)
if [ "$PWD" != "$DIR" ]
then
    echo "Only execute script from django root."
    exit -1
fi

# exit if arg count doesnt match
if [ $# -ne 2 ] or 
then
    echo "Provide python executable and django backup file to be restored"
    echo "Usage: ./blank_restore.sh python_executable django_dumpdata.json"
    exit -1
fi
python=$1
BACKUP_FILE=$2

# ask to delete
echo "If you are sure to delete the current contents of database $DB type 'sure'."
read SURE
if [ "$SURE" != "sure" ]
then
    echo "Abort."
    exit -1
else
    echo "Continues in 5 seconds..."
    sleep 5
fi


DB="nota"
USER="nota"
WORKDIR=$HOME/tmp/notaworkdir/$DB

# make working directory
mkdir -p $WORKDIR

# backup scheme
pg_dump -U $USER -v -Fc -s -f $WORKDIR/$DB.scheme $DB

#backup django data
$python manage.py dumpdata \
    --all --natural-foreign \
    --traceback \
    --output $WORKDIR/$DB.json

# drop db
dropdb -U $USER $DB

# create db
createdb --owner=$USER --encoding=UTF8 $DB

# restore scheme
pg_restore -U $USER -v -d $DB $WORKDIR/$DB.scheme

# django loaddata
$python manage.py loaddata $BACKUP_FILE
echo "Done."
echo "Your previous data are at $WORKDIR."
