import os
import sys

from PIL import Image


print(sys.argv[1])
root = sys.argv[1]
for dirname, subdirList, fileList in os.walk(root):
    for fname in fileList:
        try:
            im = Image.open(os.path.join(dirname, fname))
            # Creating resolutions...
            width = im.size[0]
            height = im.size[1]
            max_im_size = max(im.size)
            for max_size in [4000, 2000, 1000, 500, 250, 125]:
                #if(max_im_size < max_size):
                    #continue
                size = (max_size, max_size)
                im.thumbnail(size, resample=Image.BICUBIC)
                im.save(os.path.join(dirname, str(max_size) + "." + fname))
        except Exception as e:
            ext = fname.split('.')[-1]
            if (ext != 'mp4' and ext != 'MOV' and ext != 'mov' and ext != 'webm'\
                    and ext != 'm4v' and ext != 'mp3' and ext != 'MP4'):
                print(e)
sys.exit()

