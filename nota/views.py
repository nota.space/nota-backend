from django.contrib.auth.models import Group, Permission
from django.db.models import Q
from django.db import transaction
from django.shortcuts import get_object_or_404
from guardian.shortcuts import get_objects_for_user, assign_perm, remove_perm, get_perms_for_model, get_users_with_perms, get_user_perms, get_group_perms
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from accounts.models import User
from nota.models import Invite
from nota.serializers import InviteSerializer, InviteShowSerializer, GroupSerializer
from rooms.models import Room


def has_owner_permission(serializer, user):
    if serializer.validated_data['object_type'] == Invite.ObjectType.GROUP:
        group = Group.objects.get(pk=serializer.validated_data['object_id'])
        return user.has_perm("group_admin", group)
    elif serializer.validated_data['object_type'] == Invite.ObjectType.SPACE:
        room = Room.objects.get(pk=serializer.validated_data['object_id'])
        return user.has_perm("room_admin", room)
    return False


def validate_object_invite_type(serializer):
    if (serializer.validated_data['object_type'] == Invite.ObjectType.GROUP
            and serializer.validated_data['invite_type'] == Invite.InviteType.GROUP):
        raise ValidationError("You can't add a group to a group!")


class InviteViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Invite.objects.all()
    serializer_class = InviteSerializer

    def get_queryset(self):
        return super().get_queryset().filter(deleted=False)

    def create(self, request):
        data = request.data
        invite_type = data.get('invite_type')

        if invite_type == 'user':
            username = data.get( 'username' )
            if not username:
                raise ValidationError("Needs a username")

            try:
                invitee = User.objects.get(username=username)
            except User.DoesNotExist:
                raise ValidationError("User not found!")

            data['type_id'] = invitee.id
        elif invite_type == 'group':
            groupname = data.get( 'groupname' )
            if not groupname:
                raise ValidationError("Needs a group name")

            try:
                invitee = request.user.groups.get(name=groupname)
            except Group.DoesNotExist:
                raise ValidationError("Group not found!")

            data['type_id'] = invitee.id
        else:
            raise ValidationError("Needs an invite_type!")

        granted_permission_code = data.get('granted_permission_code')
        if not granted_permission_code:
            raise ValidationError("Need permission_code")

        object_type = data.get('object_type')
        object_id = data.get('object_id')
        object_cls = None
        if object_type == Invite.ObjectType.SPACE:
            object_cls = Room
            obj = Room.objects.get(id=object_id)
            if invite_type == 'user':
                perms = get_user_perms(invitee, obj)
                if perms:
                    raise ValidationError("User already has permissions!");
            elif invite_type == 'group':
                perms = get_group_perms(invitee, obj)
                if perms:
                    raise ValidationError("Group already has permissions!");
        elif object_type == Invite.ObjectType.GROUP:
            object_cls = Group
            obj = Group.objects.get(id=object_id)
            if invite_type == 'user':
                perms = get_user_perms(invitee, obj)
                if perms:
                    raise ValidationError("User already has permissions!");

        if not object_cls:
            raise ValidationError("Needs an object_type!")

        try:
            perm = get_perms_for_model(object_cls).get(codename=granted_permission_code)
            data['granted_permissions'] = [perm.id]
        except Permission.DoesNotExist:
            raise ValidationError("Permission not found!")

        serializer = self.get_serializer(data=data)
        try:
            Invite.objects.get(object_type = object_type, object_id = object_id,
                               invite_type = invite_type, type_id = invitee.id,
                               accepted=None)
            raise ValidationError('Invite already exists!')
        except Invite.DoesNotExist:
            pass

        serializer.is_valid(raise_exception=True)
        validate_object_invite_type(serializer)
        if has_owner_permission(serializer, request.user):
            serializer.save()
            return Response(data=serializer.data, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_403_FORBIDDEN)

    def retrieve(self, request, pk=None):
        invite = get_object_or_404(self.get_queryset(), pk=pk)
        if invite.can_view(request.user):
            serializer = self.get_serializer(invite)
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_403_FORBIDDEN)

    def update(self, request, pk=None):
        invite = get_object_or_404(self.get_queryset(), pk=pk)
        if invite.accepted is not None:
            raise ValidationError("You can not change an accepted/declined invite!")
        if invite.can_change(request.user):
            serializer = self.get_serializer(invite, data=request.data)
            serializer.is_valid(raise_exception=True)
            validate_object_invite_type(serializer)
            # check if user has permission on the new invite object:
            if has_owner_permission(serializer, request.user):
                serializer.save()
                return Response(data=serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_403_FORBIDDEN)

    def destroy(self, request, pk=None):
        invite = get_object_or_404(self.get_queryset(), pk=pk)
        if invite.accepted is not None:
            raise ValidationError("You can not delete an accepted/declined invite!")
        if invite.can_change(request.user):
            invite.deleted = True
            invite.save()
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_403_FORBIDDEN)

    @action(detail=True, methods=['PUT'])
    def accept(self, request, pk=None):
        invite = get_object_or_404(self.get_queryset(), pk=pk)
        if invite.accepted is not None:
            raise ValidationError("Invite has already been accepted/declined!")
        if invite.can_accept(request.user):
            if request.data['accepted'] is True:
                # save permissions:
                if invite.object_type == Invite.ObjectType.GROUP and invite.invite_type == Invite.InviteType.USER:
                    group = Group.objects.get(pk=invite.object_id)
                    user = User.objects.get(pk=invite.type_id)
                    group.user_set.add(user)
                    for permission in invite.granted_permissions.all():
                        assign_perm(permission, user, group)
                elif invite.object_type == Invite.ObjectType.SPACE:
                    room = Room.objects.get(pk=invite.object_id)
                    user_or_group = User.objects.get(
                        pk=invite.type_id) if invite.invite_type == Invite.InviteType.USER else Group.objects.get(
                        pk=invite.type_id)
                    for permission in invite.granted_permissions.all():
                        assign_perm(permission, user_or_group, room)
                invite.accepted = True
                invite.save()
            else:
                invite.accepted = False
                invite.invite_decline_text = request.data[
                    'invite_decline_text'] if 'invite_decline_text' in request.data else None
                invite.save()
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_403_FORBIDDEN)

    @action(detail=False, methods=['GET'], url_path=r'list/(?P<action_type>\w+)')
    def dashboard(self, request, action_type=None):
        if action_type == 'from':
            # sent by me:
            invites_from_me = self.get_queryset().filter(created_by=request.user)
            # room that I own:
            rooms = get_objects_for_user(request.user, 'rooms.room_admin').values_list('id', flat=True)
            # groups that I own:
            groups = get_objects_for_user(request.user, 'auth.group_admin').values_list('id', flat=True)

            invites_related = self.get_queryset().filter(
                Q(object_type=Invite.ObjectType.GROUP, object_id__in=groups)
                | Q(object_type=Invite.ObjectType.SPACE, object_id__in=rooms))
            after = request.query_params.get('after')
            if after and after.isnumeric() and after != '0':
                invites_from_me = invites_from_me.filter(id__lt=after)
                invites_related = invites_related.filter(id__lt=after)

            qs = invites_from_me.union(invites_related).order_by('-id')
            serializer = InviteShowSerializer(qs[:5], many=True)
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        elif action_type == 'to':
            invites_user = self.get_queryset().filter(invite_type=Invite.InviteType.USER, type_id=request.user.id)
            owned_group_ids = []
            for group in request.user.groups.all():
                if request.user.has_perm('group_admin', group):
                    owned_group_ids.append(group.id)
            invites_group = self.get_queryset().filter(invite_type=Invite.InviteType.GROUP, type_id__in=owned_group_ids)

            after = request.query_params.get('after')
            if after and after.isnumeric() and after != '0':
                invites_user = invites_user.filter(id__lt=after)
                invites_group = invites_group.filter(id__lt=after)

            qs = invites_user.union(invites_group).order_by('-id')
            serializer = InviteShowSerializer(qs[:5], many=True)
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)

class GroupViewSet(ModelViewSet):
    permission_classes = (IsAuthenticated,)

    def create(self, request):
        groupname = request.data.get( 'name' )
        if not groupname:
            raise ValidationError("Requires name for the group.")
        try:
            group = Group.objects.get(name = groupname)
            raise ValidationError("Group with name exists!")
        except Group.DoesNotExist:
            pass
        group = Group.objects.create(name = groupname)
        group.user_set.add(request.user)
        assign_perm('group_admin', request.user, group)

        serializer = GroupSerializer(group)
        return Response(serializer.data)

    def list(self, request):
        queryset = request.user.groups
        after = request.query_params.get('after')
        if after and after.isnumeric() and after != '0':
            queryset = queryset.filter(id__gt=after)

        serializer = GroupSerializer(queryset.all()[:40], many=True,
                context={'user': request.user})
        return Response(serializer.data)

    @action(detail=True, methods=['get'], name='Group users')
    def users(self, request, pk=None):
        group = get_object_or_404(request.user.groups, pk=pk)
        user = request.user

        if not any(request.user.has_perm(perm, group) for perm in ['group_admin', 'group_user']):
            return Response("Not authorized", status=status.HTTP_403_FORBIDDEN)

        users = group.user_set.order_by('id')

        after = request.query_params.get('after')
        if after and after.isnumeric() and after != '0':
            users = users.filter(id__gt=after)

        r = [{
                'id': user.id,
                'username': user.username,
                'permissions': get_user_perms(user, group),
            } for user in users[:40]]
        return Response(r, status=status.HTTP_200_OK)

    @transaction.atomic
    @action(detail=True, methods=['put'], name='Group permissions')
    def permissions(self, request, pk=None):
        group = get_object_or_404(request.user.groups, pk=pk)

        if not request.user.has_perm('group_admin', group):
            return Response("Not authorized", status=status.HTTP_403_FORBIDDEN)

        perm_code = request.data.get('perm_code')
        if perm_code != "none":
            try:
                new_perm = get_perms_for_model(Group).get(codename=perm_code)
            except Permission.DoesNotExist:
                raise ValidationError("Permission not found!")

        ids = request.data.get('ids')
        if not ids or len(ids) > 20:
            raise ValidationError("Need reasonable ids!")

        for k in ids:
            user = get_object_or_404(group.user_set, id=k)

            if user.id == request.user.id:
                raise ValidationError("Can't change your own permissions!")

            perms = get_user_perms(user, group)

            for perm in perms:
                remove_perm(perm, user, group)

            # indicating no replacement
            if perm_code == "none":
                group.user_set.remove(user)
            else:
                assign_perm(new_perm, user, group)

        return Response('OK', status=status.HTTP_200_OK)
