from rest_framework import serializers

from guardian.shortcuts import get_user_perms

from django.contrib.auth.models import Group
from accounts.models import User
from nota.models import Invite
from rooms.models import Room


class InvitesSerializer(serializers.ListSerializer):

    def update(self, instance, validated_data):
        pass


class InviteShowSerializer(serializers.ModelSerializer):

    class Meta:
        list_serializer_class = InvitesSerializer
        model = Invite
        fields = ('id', 'invitee', 'invite_to', 'granted_permission_codes', 'invite_text',
                  'invite_decline_text', 'accepted', 'deleted', 'created_by', 'updated_by')

    invitee = serializers.SerializerMethodField()
    invite_to = serializers.SerializerMethodField()
    granted_permission_codes = serializers.SerializerMethodField()
    created_by = serializers.SerializerMethodField()

    def get_created_by(self, invite):
        return str(invite.created_by)

    def get_invitee(self, invite):
        if invite.invite_type == Invite.InviteType.USER:
            return User.objects.get(id=invite.type_id).username
        elif invite.invite_type == Invite.InviteType.GROUP:
            return "[group: {}]".format(Group.objects.get(id=invite.type_id).name)

    def get_invite_to(self, invite):
        if invite.object_type == Invite.ObjectType.SPACE:
            room = Room.objects.get(id=invite.object_id)
            return { "type": invite.object_type, "name": room.name, "username": room.created_by.username }
        elif invite.object_type == Invite.ObjectType.GROUP:
            return { "type": invite.object_type, "name": Group.objects.get(id=invite.object_id).name }

    def get_granted_permission_codes(self, invite):
        return [p.codename for p in invite.granted_permissions.all()]

class InviteSerializer(serializers.ModelSerializer):

    class Meta:
        list_serializer_class = InvitesSerializer
        model = Invite
        fields = ('id', 'invite_type', 'type_id', 'object_type', 'object_id', 'granted_permissions', 'invite_text',
                  'invite_decline_text', 'accepted', 'deleted', 'created_by', 'updated_by')

    def create(self, validated_data):
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
            create_data = validated_data

            if 'updated_by' in create_data:
                del create_data['updated_by']
            if 'invite_decline_text' in create_data:
                del create_data['invite_decline_text']
            create_data['created_by'] = user
            create_data['accepted'] = None
            create_data['deleted'] = False

            return super().create(create_data)

    def update(self, instance, validated_data):
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
            update_data = validated_data

            if 'created_by' in update_data:
                del update_data['created_by']
            update_data['updated_by'] = user

            return super().update(instance, update_data)


class GroupsSerializer(serializers.ListSerializer):

    def update(self, instance, validated_data):
        pass


class GroupSerializer(serializers.ModelSerializer):

    class Meta:
        list_serializer_class = GroupsSerializer
        model = Group
        fields = ('id', 'name', 'permissions')

    permissions = serializers.SerializerMethodField()

    def get_permissions(self, group):
        user = self.context.get("user")

        if not user:
            return []
        else:
            return [
                p for p in get_user_perms(user, group)
                if p in ['group_admin', 'group_user']
            ]
