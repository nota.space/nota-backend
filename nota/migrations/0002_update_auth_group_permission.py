from django.contrib.auth.models import Permission, Group
from django.contrib.contenttypes.models import ContentType
from django.db import migrations


# This migration adds two permissions to Group:
# group_admin: can manage the group (add/remove people, manage group permissions)
# group_user: can only view the group
def forwards(apps, schema_editor):
    group_content_type = ContentType.objects.get_for_model(Group)
    Permission.objects.create(content_type=group_content_type, codename="group_admin", name="Group Admin")
    Permission.objects.create(content_type=group_content_type, codename="group_user", name="Group User")


class Migration(migrations.Migration):
    dependencies = [
        ('nota', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(forwards),
    ]
