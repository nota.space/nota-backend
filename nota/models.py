from django.contrib.auth.models import Permission, Group
from django.db import models
from django.utils.translation import gettext_lazy as _

from accounts.models import User
from rooms.models import Room


class Invite(models.Model):
    class InviteType(models.TextChoices):
        USER = 'user', _('User')
        GROUP = 'group', _('Group')

    class ObjectType(models.TextChoices):
        SPACE = 'space', _('Space')
        GROUP = 'group', _('Group')

    # Whether inviting a user or a group
    invite_type = models.TextField(choices=InviteType.choices, default=InviteType.USER)
    # User id or Group id:
    type_id = models.IntegerField(null=False)
    # Whether inviting to a space or to a group:
    object_type = models.TextField(choices=ObjectType.choices, default=ObjectType.GROUP)
    # Space id or Group id:
    object_id = models.IntegerField(null=False)
    # List of permissions to be granted
    granted_permissions = models.ManyToManyField(Permission, blank=True, db_table='invites_permissions')
    # True == Accepted, False == Declined
    accepted = models.BooleanField(null=True, default=None)
    invite_text = models.CharField(blank=True, max_length=255)
    invite_decline_text = models.CharField(blank=True, max_length=255)
    deleted = models.BooleanField(blank=False, default=False)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, on_delete=models.PROTECT, null=True, db_column='created_by',
                                   related_name='invite_created_by')
    updated_by = models.ForeignKey(User, on_delete=models.PROTECT, null=True, db_column='updated_by',
                                   related_name='invite_updated_by')

    class Meta:
        verbose_name = "Invite"
        verbose_name_plural = "Invites"
        db_table = 'invites'
        default_permissions = ()

    def can_change(self, user):
        if self.object_type == Invite.ObjectType.GROUP:
            group = Group.objects.get(pk=self.object_id)
            return user.has_perm("group_admin", group)
        if self.object_type == Invite.ObjectType.SPACE:
            room = Room.objects.get(pk=self.object_id)
            return user.has_perm("room_admin", room)
        return False

    def can_accept(self, user):
        if self.invite_type == Invite.InviteType.USER:
            return self.type_id == user.id
        if self.invite_type == Invite.InviteType.GROUP:
            group = Group.objects.get(pk=self.type_id)
            return user.has_perm("group_admin", group)
        return False

    def can_view(self, user):
        # TODO: please determine who can view an invite
        return self.can_change(user) or self.can_accept(user)
