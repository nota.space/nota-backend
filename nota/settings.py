import os
import sys
from dotenv import load_dotenv
import dj_database_url
from pathlib import Path

env_path = Path('.') / '.env'
load_dotenv(verbose=True, dotenv_path=env_path, override=False)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# for local dev envs
dev_env = os.getenv("DEV_ENV", False)
dev_env = dev_env == 'True'
# the pre env on server
pre_env = os.getenv("PRE_ENV", False)
pre_env = pre_env == 'True'
# the release env on server
release_env = os.getenv("RELEASE_ENV", False)
release_env = release_env == 'True'
# the name of the database to use
database = os.getenv("DATABASE")

# NEVER LOG DATABASE_URL_NO_DB
db_url_no_db = os.getenv("DATABASE_URL_NO_DB")

# if both db url without a db and database are given in environment,
# use the combined values as database url (added for test/pre database)
if db_url_no_db is not None and database is not None:
    db_url = db_url_no_db + database
else:
    print("environment variable missing:")
    if(database is None):
        print("DATABASE (should be the name of the database to use)")
    if(db_url_no_db is None):
        print("DATABASE_URL_NO_DB (should be the database url excluding the database")
    sys.stdout.flush()
    exit()

# XOR on environment settings TODO: should this be a string?
if not (dev_env ^ pre_env ^ release_env):
    print('exactly one of these environment variables must be set to true:')
    print("DEV_ENV, PRE_ENV, RELEASE_ENV")
    sys.stdout.flush()
    exit()

cur_env = None
if dev_env:
    cur_env = 'DEV'
if pre_env:
    cur_env = 'PRE'
if release_env:
    cur_env = 'RELEASE'

dbs = {}
dbs['default'] = dj_database_url.parse(db_url, conn_max_age=600)
#dbs = {
    #'default': {
        #'ENGINE': 'django.db.backends.postgresql',
        #'NAME': 'mydatabase',
        #'USER': 'mydatabaseuser',
        #'PASSWORD': 'mypassword',
        #'HOST': '127.0.0.1',
        #'PORT': '5432',
    #}
#}



SECRET_KEY = os.getenv("DJANGO_SECRET_KEY")
FILE_SALT = os.getenv("DJANGO_FILE_SALT")
DATABASES = dbs

# silk profiling settings
SILKY_PYTHON_PROFILER = True
SILKY_PYTHON_PROFILER_BINARY = True
SILKY_PYTHON_PROFILER_RESULT_PATH = os.path.join(BASE_DIR, 'profiles')
SILKY_AUTHENTICATION = True  # User must login
SILKY_AUTHORISATION = True  # User must have permissions
SILKY_MAX_REQUEST_BODY_SIZE = -1  # no limit if <0, otherwise bytes
SILKY_MAX_RESPONSE_BODY_SIZE = -1  # no limit if <0, otherwise bytes
SILKY_META = True # show performance impact of silk
SILKY_INTERCEPT_PERCENT = 1 # log only 1% of requests
SILKY_MAX_RECORDED_REQUESTS = 10**4
SILKY_MAX_RECORDED_REQUESTS_CHECK_PERCENT = 10


DOTENV = os.path.join(BASE_DIR, '.env')
#SECRET_KEY = values.SecretValue()
#FILE_SALT = values.SecretValue()
DEBUG = False
#ALLOWED_HOSTS = values.ListValue([])
DATA_UPLOAD_MAX_NUMBER_FIELDS = 5000

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # installed packages
    #'django_extensions',
    'guardian',
    'rest_framework',
    'rest_framework.authtoken',
    # apps
    'accounts.apps.AccountsConfig',
    'rooms.apps.RoomsConfig',
    'uploads',
    'channels',
    'taggit',
    'logentry_admin',
    'silk',
    'nota',
]
# add daphne for the runserver command
if dev_env:
    print('adding daphne, for dev environment (runserver)')
    INSTALLED_APPS.insert(0, 'daphne')

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    #'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'silk.middleware.SilkyMiddleware',
]

ROOT_URLCONF = 'nota.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

#WSGI_APPLICATION = 'nota.wsgi.application'
if pre_env:
    REDIS_PORT = 6380
    REDIS_DATABASE = 1
else:
    REDIS_PORT = 6379
    REDIS_DATABASE = 0

print("Start Thread: DB {}, ENV {}, redis port {}, redis db {}".format(
        database,
        cur_env,
        REDIS_PORT,
        REDIS_DATABASE))
sys.stdout.flush()

ASGI_APPLICATION = 'nota.routing.application'
CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [
                'redis://127.0.0.1:' + str(REDIS_PORT) + '/' + str(REDIS_DATABASE),
                ],
            'capacity': 1000,
        },
    },
}

databases = dbs

AUTH_USER_MODEL = 'accounts.User'

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',  # default
    'guardian.backends.ObjectPermissionBackend',
)

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoObjectPermissions'
    ]
}

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'
LANGUAGE_CODE = 'en-us'
USE_I18N = True
USE_L10N = True

TIME_ZONE = 'Europe/Berlin'
USE_TZ = True

# where to look for static files
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]
# where static files are served at
STATIC_URL = '/static/'
UPLOAD_URL = 'uploads'


if dev_env:
    MEDIA_URL = '/media/'
    MEDIA_ROOT = 'media'
    DEBUG = True
    #DATABASES = os.getenv("DATABASE_URL")
    #DATABASES = values.DatabaseURLValue()
    databases = dbs
    ALLOWED_HOSTS = ['localhost', '127.0.0.1', '192.168.0.2', '192.168.0.3', '192.168.0.4',
            '192.168.0.207']
else:
    # use server settings
    pass

#class Prod(Base):
    MEDIA_URL = '/media/'
    MEDIA_ROOT = '/var/www/media/'
    #UPLOAD_URL = 'uploads'
    BASE_URL = 'https://nota.space/'
    #DATABASES = os.getenv("DATABASE_URL")
    #DATABASES = values.DatabaseURLValue()
    databases = dbs

    CSRF_COOKIE_SECURE = True
    # where static files are served from
    STATIC_ROOT = '/home/runnota/dump/static'
    ALLOWED_HOSTS = ['archive.nota.space', 'pre.nota.space', 'pre2.nota.space', 'test.nota.space', 'nota.space', 'www.nota.space', 'itz.nota.space', 'notz.uber.space', '127.0.0.1']
    USE_X_FORWARDED_HOST = True
    FILE_UPLOAD_PERMISSIONS = 0o644

