from django.contrib import admin
from django.contrib.auth.models import Group
from guardian.admin import GuardedModelAdmin

from nota.models import Invite


class InviteAdmin(GuardedModelAdmin):

    def save_model(self, request, obj, form, change):
        if obj.id:
            obj.updated_by = request.user
            super().save_model(request, obj, form, change)
        else:
            obj.created_by = request.user
            obj.save()


admin.site.register(Invite, InviteAdmin)
admin.site.unregister(Group)
admin.site.register(Group, GuardedModelAdmin)
