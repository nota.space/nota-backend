from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from guardian.shortcuts import assign_perm

from rooms.models import Room
from .models import Invite
from accounts.models import User


class InvitationApiTestCase(APITestCase):
    def setUp(self) -> None:
        self.test_user = User.objects.create_user(username='test', password='test')
        assign_perm("nota.add_invite", self.test_user)
        assign_perm("nota.delete_invite", self.test_user)
        self.test2_user = User.objects.create_user(username='test2', password='test2')
        assign_perm("nota.view_invite", self.test2_user)
        assign_perm("nota.change_invite", self.test2_user)
        self.space = Room.objects.create(
            name="test_space",
            created_by=self.test_user,
            admin=self.test_user,
            anonymous_access=False
        )

    def test_invite_user_to_space(self):
        data = {
            "invite_type": Invite.InviteType.USER,
            "type_id": self.test2_user.id,
            "object_type": Invite.ObjectType.SPACE,
            "object_id": self.space.id,
            "created_by": self.test_user.id,
        }
        self.client.force_authenticate(self.test2_user)
        response = self.client.post(reverse("invites-list"), data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        self.client.force_authenticate(self.test_user)
        response = self.client.post(reverse("invites-list"), data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        invite = Invite.objects.filter(object_id=self.space.id).first()
        self.assertIsNotNone(invite)

        self.client.force_authenticate(self.test_user)
        response = self.client.get(reverse("invites-detail", args=[invite.id]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        self.client.force_authenticate(self.test2_user)
        response = self.client.get(reverse("invites-detail", args=[invite.id]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["object_id"], self.space.id)

        self.assertFalse(invite.accepted)

        self.client.force_authenticate(self.test_user)
        response = self.client.put(reverse("invites-detail", args=[invite.id]), {**data, "accepted": True})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        self.client.force_authenticate(self.test2_user)
        response = self.client.put(reverse("invites-detail", args=[invite.id]), {**data, "accepted": True})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        invite.refresh_from_db()
        self.assertTrue(invite.accepted)

        self.client.force_authenticate(self.test2_user)
        response = self.client.delete(reverse("invites-detail", args=[invite.id]))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        self.client.force_authenticate(self.test_user)
        response = self.client.delete(reverse("invites-detail", args=[invite.id]))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Invite.objects.filter(deleted=False, pk=invite.id).count(), 0)
