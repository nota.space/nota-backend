import os
import django
from channels.routing import get_default_application
#from configurations import importer
# from channels.asgi import get_channel_layer

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'nota.settings')
os.environ.setdefault('DJANGO_CONFIGURATION', 'Prod')
#importer.install()
from django.conf import settings

django.setup()
application = get_default_application()
# channel_layer = get_channel_layer()

