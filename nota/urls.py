from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static
from rest_framework.documentation import include_docs_urls
from rest_framework.routers import DefaultRouter

from nota.views import InviteViewSet, GroupViewSet


router = DefaultRouter()
router.register("invites", InviteViewSet, "invites")
router.register("groups", GroupViewSet, "groups")

urlpatterns = [
    path("", include(router.urls)),
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('admin/', admin.site.urls),
    path('accounts/', include('accounts.urls')),
    path('room/', include('rooms.urls')),
    path('uploads/', include('uploads.urls')),
    path('docs/', include_docs_urls(title='nota api', schema_url="/"), name="docs"),
    path('silk/', include('silk.urls', namespace='silk')),
]

if settings.DEBUG:
    urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
